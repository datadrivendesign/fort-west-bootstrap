# README #

### What is this repository for? ###

* This adds the custom bootstrap less for the fort-west templates

### How do I get set up? ###

1. Navigate to your projects assets/less directory
2. Run the following command:

```
#!shell

git submodule add git@bitbucket.org:datadrivendesign/fort-west-bootstrap.git

```

3. Add the following to your projects projectname.less file:

```
#!less

@import "fort-west-bootstrap/fort-west.less";
```

4. Initialize the submodule on your system

```
#!shell

git submodule update --init --recursive
```